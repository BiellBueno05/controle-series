<x-layout title="Nova Série">
    <form action="{{ route('series.store') }}" method="POST">
  @csrf
  <div class="row mb-3">
    <div class="mb-3 col-8">
        <label for="name" class="form-label">Nome:</label>
        <input type="text" autofocus id="name" name="name" class="form-control" >
    </div>

    <div class="mb-3 col-2">
        <label for="seasonsQty" class="form-label">N Temporadas:</label>
        <input type="text" id="seasonsQty" name="seasonsQty" class="form-control">
    </div>

    <div class="mb-3 col-2">
        <label for="episodesPerSeason" class="form-label">Eps/ temporadas:</label>
        <input type="text" id="episodesPerSeason" name="episodesPerSeason" class="form-control" >
    </div>
  </div>
  <button type="submit" class="btn btn-primary">Adicionar</button>
</form>
<a href="{{route('series.index')}}" type="button" class="btn btn-dark badge mt-3">Voltar</a>
</x-layout>
