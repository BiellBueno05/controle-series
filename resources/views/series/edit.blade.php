<x-layout title="Editar Série {!!$series->name!!}">
    <x-series.form :action="route('series.update', $series->id)" :name="$series->name" />
    <a href="{{route('series.index')}}" type="button" class="btn btn-dark badge mt-3">Voltar</a>
</x-layout>