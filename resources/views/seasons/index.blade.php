<x-layout title="Temporadas de {!!$series->name!!}">

    <ul class="list-group">
        @foreach ($seasons as $season)
        <li class="list-group-item d-flex justify-content-between aling-itens-center">
            <a href="{{route('episodes.index', $season->id)}}">
                Temporada {{ $season->number }}
            </a>

            <span class="badge bg-secondary">

                 {{ $season->episodes->filter(fn ($episode) => $episode->watched)->count() }} /{{$season->episodes->count()}}

            </span>
        
        </li>

        @endforeach
    </ul>
     <a href="{{route('series.index')}}" type="button" class="btn btn-dark badge mt-3">Voltar</a>

</x-layout>
