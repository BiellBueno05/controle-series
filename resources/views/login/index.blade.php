<!doctype html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>
<body class="">

  <div class="container text-center">
    <h1>Login</h1>
      <form method="post">
        @csrf
        <div class="form-group">
          <label for="email" class="  form-label">Email</label>
          <input type="email" class=" form-control" id="email" name="email">
        </div>
        <div class="form-group">
          <label for="password" class="  form-label">Senha</label>
          <input type="password" class=" form-control" id="password" name="password">
        </div>
        <button class="btn btn-primary mt-3">
          Entrar
        </button>
      </form>
      <a href="{{ route('users.create') }}" class="btn btn-secondary mt-3">
        Registrar-se
      </a>
  
  </div>

</body>
</html>