<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\SeriesFormRequest;
use App\Models\Series;
use App\Models\Seasons;
use App\Models\Episode;
use \App\Http\Controllers\SeasonsController;
use Illuminate\Support\Facades\Mail;
use App\Mail\SeriesCreated;
use App\Models\User;
use App\Events\SeriesCreatedEvent;

use App\Repositories\SeriesRepository;
use Illuminate\Support\Facades\DB;

class SeriesController extends Controller
{
    public function index(Request $request)
    {
        $series = Series::query()->orderBy('name')->get();
        $mensagemSucesso = session()->get('mensagem');


        return view('series.index')->with('series', $series)->with('mensagemSucesso', $mensagemSucesso);
    }

    public function create()
    {
        return view('series.create');
    }

    public function store(SeriesFormRequest $request, SeriesRepository $repository)
    {  
        $series = $repository->add($request);
        SeriesCreatedEvent::dispatch(
            $series->name,
            $series->id,
            $request->seasonsQty,
            $request->episodesPerSeason);




        return redirect()->route('series.index')->with('mensagem', "series adicionada com sucesso");
    }

    public function destroy(Series $series)
    {
        $series->delete();

        return redirect()->route('series.index')->with('mensagem', "series '{$series->name}'excluida com sucesso");
    }

    public function edit (Series $series) 
    {
        return view('series.edit')->with('series', $series);
    }

    public function update(Series $series, SeriesFormRequest $request)
    {
        $series->fill($request->all());
        $series->save();

        return redirect()->route('series.index')->with('mensagem', "series '{$series->name}' Atualizada com sucesso");
    }

    
}