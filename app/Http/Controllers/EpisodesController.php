<?php

namespace App\Http\Controllers;

use App\Http\Controllers\SeasonsController;
use App\Models\Series;
use App\Models\Seasons;
use App\Models\Episode;

use Illuminate\Http\Request;

class EpisodesController extends Controller
{
    public function index(Seasons $seasons)
    {
        return view('episodes.index', [
            'episodes' => $seasons->episodes,
            'mensagemSucesso' => session('mensagem.sucesso')
        ]);
    }

    public function update(Request $request, Seasons $seasons)
    {
        
        $watchedEpisodes = $request->episodes;
        $seasons->episodes->each(function (Episode $episode) use ($watchedEpisodes) {
            $episode->watched = in_array($episode->id, $watchedEpisodes);
        });

        $seasons->push();

        return redirect()->route('episodes.index', $seasons->id)->with('mensagem', "series adicionada com sucesso");
    }

}
