<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\SeriesFormRequest;
use App\Models\Series;
use App\Models\Seasons;
use App\Models\Episode;
use \App\Http\Controllers\SeriesController;
use \App\Http\Controllers\EpisodesController;




class SeasonsController extends Controller
{
    public function index(Series $series)
    {
        // dd($series);
        $seasons = $series->seasons()->with('episodes')->get();

        return view('seasons.index')->with('seasons', $seasons)->with('series', $series);
    }
}
