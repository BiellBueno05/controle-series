<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SeriesFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return True;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'min:2']
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'O campo nome é obrigatório e deve possuir no minimo 2 caracteres.',
            'name.min' =>"O campo deve possuir no minimo 2 caracteres."
        ];
    }
}