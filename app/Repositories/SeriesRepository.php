<?php

namespace App\Repositories;

use App\Http\Requests\SeriesFormRequest;
use App\Models\Series;

Interface SeriesRepository
{
  public function add(SeriesFormRequest $request) : Series;

}